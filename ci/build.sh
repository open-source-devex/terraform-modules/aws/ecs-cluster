#!/usr/bin/env sh

set -e

if [ -f ".env" ]; then
  source .env
fi

set -v

CI_VARIABLES=ci-variables.tfvars
echo '
vpc_id = "string"
vpc_private_subnet_ids = ["list"]
key_name = "string"
cloud_config_content = "string"
' > ${CI_VARIABLES}

OVERRIDES_FILE=ci-overrides.tf
echo '
provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
