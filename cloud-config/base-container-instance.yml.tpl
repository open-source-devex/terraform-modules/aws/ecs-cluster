#cloud-config

bootcmd:
  - mkdir -p /etc/ecs
  - echo 'ECS_CLUSTER=${ecs_cluster_name}' >> /etc/ecs/ecs.config
  - echo 'ECS_ENABLE_CONTAINER_METADATA=${ecs_enable_metadata}' >> /etc/ecs/ecs.config
  - echo 'ECS_CONTAINER_STOP_TIMEOUT=${ecs_container_stop_timeout}' >> /etc/ecs/ecs.config
  - echo "ENVIRONMENT=${environment}" > /etc/profile.d/00-environment.sh
