# #################################################################
# Security Group rules for ECS instances
# #################################################################

# Services on ECS instances to each other
resource "aws_security_group_rule" "ecs_instances_app_in" {
  description = "Application traffic from ECS instances"

  type                     = "ingress"
  from_port                = 32768
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.container_instance.id}"

  security_group_id = "${aws_security_group.container_instance.id}"
}

resource "aws_security_group_rule" "ecs_instances_app_out" {
  description = "Application traffic to ECS instances"

  type                     = "egress"
  from_port                = 32768
  to_port                  = 65535
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.container_instance.id}"

  security_group_id = "${aws_security_group.container_instance.id}"
}

# SSH <= in
resource "aws_security_group_rule" "ecs_instances_ssh_in" {
  count = "${var.allow_ssh_in ? length(keys(var.ssh_allowed_cidrs)) : 0}"

  description = "from ${element(keys(var.ssh_allowed_cidrs), count.index)}"

  type      = "ingress"
  from_port = 22
  to_port   = 22
  protocol  = "tcp"

  cidr_blocks = ["${element(values(var.ssh_allowed_cidrs), count.index)}"]

  security_group_id = "${aws_security_group.container_instance.id}"
}

# HTTP => out
resource "aws_security_group_rule" "ecs_instances_http_out" {
  count = "${var.allow_http_out ? length(keys(var.http_allowed_cidrs)) : 0}"

  description = "to ${element(keys(var.http_allowed_cidrs), count.index)}"

  type      = "egress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  cidr_blocks = ["${element(values(var.http_allowed_cidrs), count.index)}"]

  security_group_id = "${aws_security_group.container_instance.id}"
}

# HTTPS => out
resource "aws_security_group_rule" "ecs_instances_https_out" {
  count = "${var.allow_https_out ? length(keys(var.https_allowed_cidrs)) : 0}"

  description = "to ${element(keys(var.https_allowed_cidrs), count.index)}"

  type      = "egress"
  from_port = 443
  to_port   = 443
  protocol  = "tcp"

  cidr_blocks = ["${element(values(var.https_allowed_cidrs), count.index)}"]

  security_group_id = "${aws_security_group.container_instance.id}"
}
